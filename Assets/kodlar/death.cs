﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class death : MonoBehaviour {

    public GameObject ok;
    public int level,levelekstra;
    public Text leveltxt;

    void Start () {
		level = PlayerPrefs.GetInt("level");
        levelekstra = level + 1;
	}
	
	// Update is called once per frame
	void Update () {
        
        leveltxt.text = levelekstra.ToString();
	}
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "engel1")
        {
            ok.gameObject.SetActive(false);

        }
        if (other.gameObject.name == "engel2")
        {
            Application.LoadLevel(3);
        }
        

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "level_gecis")
        {
            level = level + 1;
            PlayerPrefs.SetInt("level", level);
            Application.LoadLevel(4);
        }
    }
}

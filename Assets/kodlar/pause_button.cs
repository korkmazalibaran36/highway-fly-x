﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pause_button : MonoBehaviour {

    public int a;
    public GameObject panel,pausego;


    public void anamenu () {
        Application.LoadLevel(1);
	}
	
	public void pause () {
        Time.timeScale = 0;
        panel.gameObject.SetActive(true);
        pausego.gameObject.SetActive(false);
	}
    public void moveon()
    {
        Time.timeScale = 1;
        panel.gameObject.SetActive(false);
        pausego.gameObject.SetActive(true);
    }
    public void tekrar()
    {
        Application.LoadLevel(2);
    }
    
    
}

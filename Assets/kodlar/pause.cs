﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pause : MonoBehaviour {

    public GameObject    sesiac, sesikapat;
    public int ses;
     void Start()
    {
        ses = PlayerPrefs.GetInt("ses");
    }

    public void pausebut () {
        Time.timeScale = 0;
        if (ses == 0)
        {
            sesiac.gameObject.SetActive(false);
            sesikapat.gameObject.SetActive(true);
        }
        else if (ses == 1)
        {
            sesiac.gameObject.SetActive(true);
            sesikapat.gameObject.SetActive(false);
        }

    }

    public void sesac()
    {

        ses = 0;
        PlayerPrefs.SetInt("ses", ses);
        sesiac.gameObject.SetActive(false);
        sesikapat.gameObject.SetActive(true);
    }
    public void seskapa()
    {
        ses = 1;
        PlayerPrefs.SetInt("ses", ses);
        sesiac.gameObject.SetActive(true);
        sesikapat.gameObject.SetActive(false);
    }
   
    

}

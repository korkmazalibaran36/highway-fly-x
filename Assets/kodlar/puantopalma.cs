﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class puantopalma : MonoBehaviour {

    public int score,timeyardimci, bestscore, time;
    public Text txtzaman,scoretxt;

	void Start () {
        bestscore = PlayerPrefs.GetInt("bestscore");
        
	}

	
	void FixedUpdate () {
        if (score > bestscore) {
            bestscore = score;
            PlayerPrefs.SetInt("bestscore", bestscore);
        }
        timeyardimci = time / 60;
        txtzaman.text = timeyardimci.ToString();
        scoretxt.text = score.ToString();
        time -= 1;
        if(time<=6000 && time>= 3000)
        {
            txtzaman.GetComponent<Text>().color = Color.cyan;
        }
        if (time <= 3000 && time >= 1200)
        {
            txtzaman.GetComponent<Text>().color = Color.yellow;
        }
        if (time <= 1200)
        {
            txtzaman.GetComponent<Text>().color = Color.red;
        }
        PlayerPrefs.SetInt( "score",score );
        if(time == 0)
        {
            Application.LoadLevel(9);
        }

    }
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.name == "hedef") {
            score = score + 25;
            other.gameObject.SetActive(false);
        }
        if (other.gameObject.name == "engel2")
        {
            Application.LoadLevel(3);
        }
    }
}
